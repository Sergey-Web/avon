<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Avon</title>
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<script src="js/jquery-3.2.0.min.js"></script>
</head>
<body>
	<header class="header">		
		<div class="wrap-menu">
			<div class="wrap-logo">
				<a class="wrap-logo__link-logo" href="/">
					<img class="logo" src="img/logo.png" alt="logo">
				</a>
				<img class="wrap-logo__img-element" src="img/element-menu-1.png" alt="logo">
			</div>
			<div class="container-menu">
				<div class="menu">
					<img class="menu__img-back" src="img/element-menu-2.png" alt="">
					<img class="menu__icon-m1" src="img/m1.png" alt="">
					<div class="menu__timer">
						<div class="start-stage">до нового этапа осталось</div>
						<span class="menu__days">05</span>:
						<span class="menu__hours">12</span>:
						<span class="menu__minutes">45</span>:
						<span class="menu__seconds">13</span>
					</div>
					<nav class="menu__nav-sections nav-sections">
						<ul class="nav-sections__list">
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-brand" href="#">О бренде</a>
							</li>
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-rules" href="#">Правила</a>
							</li>
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-blogers" href="#">Блогеры</a>
								<ul class="nav-sections__submenu">
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="bloger.php">Лиза Краснова</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="bloger.php">Наташа Шелягина</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="bloger.php">Диана Глостер</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="bloger.php">Лена Блонда</a>
									</li>
								</ul>
							</li>
						</ul>
					</nav>
					<div class="menu__wrap-loupe">
						<img class="menu__loupe" src="img/loupe.png" alt="loupe">
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="hashtag-video">
		<h2 class="hashtag-video__title">
			#Наважся вража<span class="hashtag-video__title_red">ти</span>
		</h2>
	</div>
	<div class="wrap-container">
		<div class="container">
			<div class="container-elements">
				<img class="background-element-1" src="img/element-video-6.png" alt="">
				<img class="background-element-2" src="img/element-video-7.png" alt="">
				<img class="background-element-3" src="img/stage-competition-icon-1.png" alt="">
				<img class="background-element-4" src="img/stage-competition-icon-5.png" alt="">
			</div>
			<div class="wrap-video">
				<img class="wrap-video__element-video-1" src="img/element-video-1.png" alt="">
				<img class="wrap-video__element-video-2" src="img/element-video-2.png" alt="">
				<img class="wrap-video__element-video-3" src="img/element-video-3.png" alt="">
				<div class="wrap-video__wrap-further-more">
					<span class="wrap-video__further">дальше</span>
					<span class="wrap-video__more">больше!</span>
					<img class="wrap-video__arrow" src="img/arrow-down.png" alt="">
				</div>
				<img class="wrap-video__element-video-4" src="img/element-video-4.png" alt="">
				<img class="wrap-video__element-video-5" src="img/element-video-5.png" alt="">
				<iframe width="100%" height="450" src="https://www.youtube.com/embed/dQw4w9WgXcQ" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="wrap-main-title">
				<h1 class="wrap-main-title__title">Участвуй в бьюти челендж рейс от</h1>
				<img class="wrap-main-title__logo" src="img/logo.png" alt="">
			</div>
		</div>
	</div>
	<div class="rules-participation">
		<img class="rules-participation__element-1" src="img/stage-competition-icon-2.png">
		<img class="rules-participation__element-2" src="img/stage-competition-icon-3.png">
		<img class="rules-participation__element-3" src="img/stage-competition-icon-4.png">
		<div class="rules-participation__path-1">
			<div class="rules-participation__wrap-description">
				<p class="rules-participation__description">
					Присоединись <b>к команде</b></br> любимых блогеров и </br><b>выполняй челенжи</b>
				</p>
			</div>
		</div>
		<div class="rules-participation__path-2">
			<div class="rules-participation__wrap-description">
				<p class="rules-participation__description">
					Загружай свои работы</br> на протяжении </br><b>8 этапов</b>
				</p>
			</div>
		</div>
		<div class="rules-participation__path-3">
			<div class="rules-participation__wrap-description">
				<p class="rules-participation__description">
					Собирай <b>лайки и </br>выигрывай</b> вместе со </br>своей командой!
				</p>
			</div>
		</div>
	</div>
	<div class="wrap-container">
		<div class="container">
			<div class="navigation-stages">
				<div class="navigation-stages__stages">
					<a href="#">
						<img class="navigation-stages__prev" src="img/navigation-stages-arrow.png" alt="">
					</a>
					<a class="navigation-stages__stage" href="#">
						<span class="navigation-stages__title">Этап 2</span>
						<span class="navigation-stages__status">(завершен)</span>
					</a>
					<a class="navigation-stages__stage" href="#">
						<span class="navigation-stages__title">Этап 3</span>
						<span class="navigation-stages__status">(завершен)</span>
					</a>
					<a class="navigation-stages__stage" href="#">
						<span class="navigation-stages__title">Этап 4</span>
						<span class="navigation-stages__status">(завершен)</span>
					</a>
				</div>
				<div class="navigation-stages__wrap-action-stage">
					<img class="navigation-stages__element-1" src="img/navigation-stages-element.png" alt="">
					<div class="navigation-stages__action-stage">
						<span class="navigation-stages__action-stage-title">Этап</span>
						<span class="navigation-stages__action-stage-num">5</span>
					</div>
					<span class="navigation-stages__line"></span>
				</div>
			</div>

			<div class="task-blogers">
				<div class="task-blogers__wrap-item">				
					<section class="task-blogers__bloger-section bloger-section">
						<div class="bloger-section__task">
							<div class="bloger-section__title">
								<span class="bloger-section__title-task">задание</span>
								<span class="bloger-section__title-stage">этапа</span>
							</div>
							<span class="bloger-section__description-task">Весь макияж только губной помадой</span>
						</div>
						<div class="bloger-section__bloger">
							<div class="bloger-section__bloger-wrap-name">
								<div class="bloger-section__bloger-name">									
									<span class="bloger-section__first-name-bloger">Лиза</span>
									<span class="bloger-section__last-name-bloger">Краснова</span>
								</div>
							</div>
							<img class="bloger-section__bloger-img" src="img/leader-team-LK.png" alt="">
							<div class="like">
								<div class="like__container">
									<span class="like__count">2597</span>
									<img class="like__icon" src="img/task-stage-icon-like.png">
								</div>
							</div>
						</div>
						<a class="bloger-section__wrap-video" href="#"><img class="bloger-section__video" src="img/bloger-video.jpg" alt=""></a>
						<div class="bloger-section__team team">
							<span class="team__title">команда блогера</span>
							<span class="team__name">143</span>
						</div>
					</section>
					<section class="task-blogers__bloger-section bloger-section">
						<div class="bloger-section__task">
							<div class="bloger-section__title">
								<span class="bloger-section__title-task">задание</span>
								<span class="bloger-section__title-stage">этапа</span>
							</div>
							<span class="bloger-section__description-task">Весь макияж только губной помадой</span>
						</div>
						<div class="bloger-section__bloger">
							<div class="bloger-section__bloger-wrap-name">
								<div class="bloger-section__bloger-name">									
									<span class="bloger-section__first-name-bloger">Лиза</span>
									<span class="bloger-section__last-name-bloger">Краснова</span>
								</div>
							</div>
							<img class="bloger-section__bloger-img" src="img/leader-team-NSH.png" alt="">
							<div class="like">
								<div class="like__container">
									<span class="like__count">2597</span>
									<img class="like__icon" src="img/task-stage-icon-like.png">
								</div>
							</div>
						</div>
						<a class="bloger-section__wrap-video" href="#"><img class="bloger-section__video" src="img/bloger-video.jpg" alt=""></a>
						<div class="bloger-section__team team">
							<span class="team__title">команда блогера</span>
							<span class="team__name">143</span>
						</div>
					</section>
				</div>
				<div class="task-blogers__wrap-item">				
					<section class="task-blogers__bloger-section bloger-section">
						<div class="bloger-section__task">
							<div class="bloger-section__title">
								<span class="bloger-section__title-task">задание</span>
								<span class="bloger-section__title-stage">этапа</span>
							</div>
							<span class="bloger-section__description-task">Весь макияж только губной помадой</span>
						</div>
						<div class="bloger-section__bloger">
							<div class="bloger-section__bloger-wrap-name">
								<div class="bloger-section__bloger-name">									
									<span class="bloger-section__first-name-bloger">Лиза</span>
									<span class="bloger-section__last-name-bloger">Краснова</span>
								</div>
							</div>
							<img class="bloger-section__bloger-img" src="img/leader-team-LK.png" alt="">
							<div class="like">
								<div class="like__container">
									<span class="like__count">2597</span>
									<img class="like__icon" src="img/task-stage-icon-like.png">
								</div>
							</div>
						</div>
						<a class="bloger-section__wrap-video" href="#"><img class="bloger-section__video" src="img/bloger-video.jpg" alt=""></a>
						<div class="bloger-section__team team">
							<span class="team__title">команда блогера</span>
							<span class="team__name">143</span>
						</div>
					</section>
					<section class="task-blogers__bloger-section bloger-section">
						<div class="bloger-section__task">
							<div class="bloger-section__title">
								<span class="bloger-section__title-task">задание</span>
								<span class="bloger-section__title-stage">этапа</span>
							</div>
							<span class="bloger-section__description-task">Весь макияж только губной помадой</span>
						</div>
						<div class="bloger-section__bloger">
							<div class="bloger-section__bloger-wrap-name">
								<div class="bloger-section__bloger-name">									
									<span class="bloger-section__first-name-bloger">Лиза</span>
									<span class="bloger-section__last-name-bloger">Краснова</span>
								</div>
							</div>
							<img class="bloger-section__bloger-img" src="img/leader-team-NSH.png" alt="">
							<div class="like">
								<div class="like__container">
									<span class="like__count">2597</span>
									<img class="like__icon" src="img/task-stage-icon-like.png">
								</div>
							</div>
						</div>
						<a class="bloger-section__wrap-video" href="#"><img class="bloger-section__video" src="img/bloger-video.jpg" alt=""></a>
						<div class="bloger-section__team team">
							<span class="team__title">команда блогера</span>
							<span class="team__name">143</span>
						</div>
					</section>
				</div>
				<img class="task-blogers__element-1" src="img/element-team-1.png" alt="">
				<img class="task-blogers__element-2" src="img/element-team-2.png" alt="">
			</div>
			<div class="container-elements">				
				<img class="background-element-5" src="img/bottom-elment-1.png">
				<img class="background-element-6" src="img/bottom-elment-2.png">
			</div>
		</div>
	</div>

</body>
</html>