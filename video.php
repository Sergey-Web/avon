<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Avon</title>
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<script src="js/jquery-3.2.0.min.js"></script>
</head>
<body>
	<header class="header">		
		<div class="wrap-menu">
			<div class="wrap-logo">
				<a class="wrap-logo__link-logo" href="/">
					<img class="logo" src="img/logo.png" alt="logo">
				</a>
				<img class="wrap-logo__img-element" src="img/element-menu-1.png" alt="logo">
			</div>
			<div class="container-menu">
				<div class="menu">
					<img class="menu__img-back" src="img/element-menu-2.png" alt="">
					<img class="menu__icon-m1" src="img/m1.png" alt="">
					<div class="menu__timer">
						<div class="start-stage">до нового этапа осталось</div>
						<span class="menu__days">05</span>:
						<span class="menu__hours">12</span>:
						<span class="menu__minutes">45</span>:
						<span class="menu__seconds">13</span>
					</div>
					<nav class="menu__nav-sections nav-sections">
						<ul class="nav-sections__list">
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-brand" href="#">О бренде</a>
							</li>
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-rules" href="#">Правила</a>
							</li>
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-blogers" href="#">Блогеры</a>
								<ul class="nav-sections__submenu">
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Лиза Краснова</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Наташа Шелягина</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Диана Глостер</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Лена Блонда</a>
									</li>
								</ul>
							</li>
						</ul>
					</nav>
					<div class="menu__wrap-loupe">
						<img class="menu__loupe" src="img/loupe.png" alt="loupe">
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="wrap-container">
		<div class="container">
			<div class="container-elements">
				<img class="bloger-element-1" src="img/bloger-element-1.png" alt="">
				<img class="video-element-1" src="img/element-decoration-6.png" alt="">
				<img class="video-element-2" src="img/video-element-1.png" alt="">
				<img class="video-element-3" src="img/stage-competition-icon-5.png" alt="">
				<img class="bloger-element-6" src="img/element-decoration-7.png" alt="">
				<img class="bloger-element-7" src="img/element-video-6.png" alt="">
			</div>
			<div class="title-stage">
				<div class="title-stage__wrap-stage">
					<div class="title-stage__inner-wrap-stage">						
						<span class="title-stage__stage">Этап</span>
						<span class="title-stage__num">5</span>
					</div>
				</div>
				<div class="title-stage__line"></div>
				<span class="title-stage__name-team">команда Лики Пай</span>
			</div>
		</div>
		<h2 class="presentation-title">Весь макияж только хайлайтером</h2>
		<div class="example-work-wrap">
			<div class="container">
				<div class="example-work-inner-wrap">					
					<article class="example-work">
						<div class="example-work__wrap-video">
							<iframe class="example-work__video" width="100%" height="264" src="https://www.youtube.com/embed/34Na4j8AVgA" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="example-work__desc">
							<h3 class="example-work__author">Виктория Лун</h3>
							<h4 class="example-work__title">Весенний хайлайт-макияж</h4>
							<p class="example-work__desc-article">
								Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.
							</p>
							<div class="example-work__like">
								<span class="example-work__count-like">24</span>
								<img class="example-work__icon-like" src="img/icon-like-black.png" alt="">
							</div>
						</div>
					</article>
					<div class="example-work__social-icons">
						<a href="#"><img class="example-work__icon-vk" src="img/icon-vk.png" alt=""></a>
						<a href="#"><img class="example-work__icon-facebook" src="img/icon-facebook.png" alt=""></a>
						<a href="#"><img class="example-work__icon-instagram" src="img/icon-instagram.png" alt=""></a>
					</div>
				</div>		
			</div>
		</div>
		<div class="title-comments">
			<div class="title-comments__text">Комментарии</div>
			<div class="title-comments__line"></div>
		</div>
		<div class="container">
			<div class="container-comments">
				<form class="form-comments-video" enctype="multipart/form-data" method="post" action="">
					<img class="form-comments-video__photo-commentator" src="img/commentator.png" alt="">
					<div class="form-comments-video__header">
						<div class="form-comments-video__wrap-comment">
							<input type="text" class="form-comments-video__comment">
						</div>
						<div class="form-comments-video__bottom">
							<div class="form-comments-video__addition">							
								<label class="form-comments-video__lable-subscription form-comments-video__item">
									<input type="checkbox" class="form-comments-video__subscription">
									<span class="form-comments-video__subscription-text">Подписатся на все комментарии</span>
								</label>
								<label class="form-comments-video__label-reply form-comments-video__item">
									<input type="checkbox" class="form-comments-video__reply">
									<span class="form-comments-video__reply-text">Получать ответ на мой комментарий</span>
								</label>
								<img class="form-comments-video__quote form-comments-video__item" src="img/quote.png" alt="">
								<label class="form-comments-video__label-file form-comments-video__item">
									<img class="form-comments-video__icon-file" src="img/icon-img.png" alt="">
									<input type="file" class="form-comments-video__file">
								</label>
							</div>
							<div class="form-comments-video__wrap-button">
								<button type="submit" class="form-comments-video__button">Отправить комментарий</button>
							</div>
						</div>
					</div>
				</form>
				<div class="comment-video">
					<img class="comment-video__photo-author" src="img/commentator.png" alt="">
					<div class="comment-video__wrap-author">
						<div class="comment-video__author">
							<span class="comment-video__author-name">Юлия</span>
							<span class="comment-video__date">18:45 06.02.2013</span>
						</div>
						<p class="comment-video__comment">Процесс привыкания друг к другу, а это согласитесь. Тем, что идеальных семей.</p>
						<div class="comment-video__wrap-like">
							<div class="comment-video__like">						
								<span class="comment-video__coutn-like">+ 17</span>
								<img class="comment-video__like-positive" src="img/icon-like-positive.png" alt="">
								<img class="comment-video__like-negative" src="img/icon-like-negative.png" alt="">
							</div>
							<div class="comment-video__wrap-reply">
								<div class="comment-video__reply">
									<img class="comment-video__icon-reply" src="img/icon-reply.png" alt="">
									<span class="comment-video__text-reply">Ответить</span>
								</div>
								<div class="comment-video__subscription">
									<img class="comment-video__icon-subscription" src="img/icon-share.png" alt="">
									<span class="comment-video__text-subscription">Подписаться</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="comment-video">
					<img class="comment-video__photo-author" src="img/commentator.png" alt="">
					<div class="comment-video__wrap-author">
						<div class="comment-video__author">
							<span class="comment-video__author-name">Юлия</span>
							<span class="comment-video__date">18:45 06.02.2013</span>
						</div>
						<p class="comment-video__comment">Процесс привыкания друг к другу, а это согласитесь. Тем, что идеальных семей.</p>
						<div class="comment-video__wrap-like">
							<div class="comment-video__like">						
								<span class="comment-video__coutn-like">+ 17</span>
								<img class="comment-video__like-positive" src="img/icon-like-positive.png" alt="">
								<img class="comment-video__like-negative" src="img/icon-like-negative.png" alt="">
							</div>
							<div class="comment-video__wrap-reply">
								<div class="comment-video__reply">
									<img class="comment-video__icon-reply" src="img/icon-reply.png" alt="">
									<span class="comment-video__text-reply">Ответить</span>
								</div>
								<div class="comment-video__subscription">
									<img class="comment-video__icon-subscription" src="img/icon-share.png" alt="">
									<span class="comment-video__text-subscription">Подписаться</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="comment-video">
					<img class="comment-video__photo-author" src="img/commentator.png" alt="">
					<div class="comment-video__wrap-author">
						<div class="comment-video__author">
							<span class="comment-video__author-name">Юлия</span>
							<span class="comment-video__date">18:45 06.02.2013</span>
						</div>
						<p class="comment-video__comment">Процесс привыкания друг к другу, а это согласитесь. Тем, что идеальных семей.</p>
						<div class="comment-video__wrap-like">
							<div class="comment-video__like">						
								<span class="comment-video__coutn-like">+ 17</span>
								<img class="comment-video__like-positive" src="img/icon-like-positive.png" alt="">
								<img class="comment-video__like-negative" src="img/icon-like-negative.png" alt="">
							</div>
							<div class="comment-video__wrap-reply">
								<div class="comment-video__reply">
									<img class="comment-video__icon-reply" src="img/icon-reply.png" alt="">
									<span class="comment-video__text-reply">Ответить</span>
								</div>
								<div class="comment-video__subscription">
									<img class="comment-video__icon-subscription" src="img/icon-share.png" alt="">
									<span class="comment-video__text-subscription">Подписаться</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>