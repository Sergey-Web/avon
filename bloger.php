<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Avon</title>
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<script src="js/jquery-3.2.0.min.js"></script>
</head>
<body>
	<header class="header">		
		<div class="wrap-menu">
			<div class="wrap-logo">
				<a class="wrap-logo__link-logo" href="/">
					<img class="logo" src="img/logo.png" alt="logo">
				</a>
				<img class="wrap-logo__img-element" src="img/element-menu-1.png" alt="logo">
			</div>
			<div class="container-menu">
				<div class="menu">
					<img class="menu__img-back" src="img/element-menu-2.png" alt="">
					<img class="menu__icon-m1" src="img/m1.png" alt="">
					<div class="menu__timer">
						<div class="start-stage">до нового этапа осталось</div>
						<span class="menu__days">05</span>:
						<span class="menu__hours">12</span>:
						<span class="menu__minutes">45</span>:
						<span class="menu__seconds">13</span>
					</div>
					<nav class="menu__nav-sections nav-sections">
						<ul class="nav-sections__list">
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-brand" href="#">О бренде</a>
							</li>
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-rules" href="#">Правила</a>
							</li>
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-blogers" href="#">Блогеры</a>
								<ul class="nav-sections__submenu">
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Лиза Краснова</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Наташа Шелягина</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Диана Глостер</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Лена Блонда</a>
									</li>
								</ul>
							</li>
						</ul>
					</nav>
					<div class="menu__wrap-loupe">
						<img class="menu__loupe" src="img/loupe.png" alt="loupe">
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="wrap-container">
		<div class="container">
			<div class="container-elements">
				<img class="bloger-element-1" src="img/bloger-element-1.png" alt="">
				<img class="bloger-element-2" src="img/stage-competition-icon-5.png" alt="">
				<img class="bloger-element-3" src="img/element-decoration-6.png" alt="">
				<img class="bloger-element-4" src="img/element-decoration-2.png" alt="">
				<img class="bloger-element-5" src="img/element-decoration-3.png" alt="">
				<img class="bloger-element-6" src="img/element-decoration-7.png" alt="">
				<img class="bloger-element-7" src="img/element-video-6.png" alt="">
			</div>
			<div class="wrap-statistics-bloger">
				<div class="statistics-bloger">
					<div class="statistics-bloger__like">
						<span class="statistics-bloger__like-count">12345</span>
						<img class="statistics-bloger__icon" src="img/task-stage-icon-like.png" alt="">
					</div>
					<div class="statistics-bloger__wrap-count-team">
						<span class="statistics-bloger__count-team">735</span>
						<span class="staticstics-bloger__text-team">человек<br> в команде</span>
					</div>
				</div>
				<div class="button-challenge">
					<a class="button-challenge__join-team" href="#">присоединится к команде</a>
					<a class="button-challenge__all" href="#">все челенджи</a>
				</div>
			</div>
		</div>
		<div class="feature-bloger">
			<div class="container">
				<div class="feature-bloger__wrap-img">
					<img class="feature-bloger__img" src="img/leader-team-DG-shadow.png" alt="">
				</div>
				<div class="feature-bloger__wrap-desc">
					<div class="feature-bloger__wrap-name">
						<span class="feature-bloger__specialty">бьюти блогер</span>
						<span class="feature-bloger__first-name">Диана</span>
						<span class="feature-bloger__last-name">Глостер</span>
					</div>
					<p class="feature-bloger__desc">
						Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.
					</p>
				</div>
				<div class="social-icons">
					<a href="#"><img class="social-icons__vk" src="img/icon-vk.png" alt=""></a>
					<a href="#"><img class="social-icons__facebook" src="img/icon-facebook.png" alt=""></a>
					<a href="#"><img class="social-icons__instagram" src="img/icon-instagram.png" alt=""></a>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="navigation-stages">
				<div class="navigation-stages__stages">
					<img class="navigation-stages__prev" src="img/navigation-stages-arrow.png" alt="">
					<a class="navigation-stages__stage navigation-stages__stage_active" href="#">
						<span class="navigation-stages__title">Этап 3</span>
						<span class="navigation-stages__status">(завершен)</span>
					</a>
					<a class="navigation-stages__stage" href="#">
						<span class="navigation-stages__title">Этап 4</span>
						<span class="navigation-stages__status">(завершен)</span>
					</a>
					<a class="navigation-stages__stage" href="#">
						<span class="navigation-stages__title">Этап 5</span>
						<span class="navigation-stages__status">(завершен)</span>
					</a>
				</div>
				<div class="navigation-stages__wrap-action-stage">
					<img class="navigation-stages__element-1" src="img/navigation-stages-element.png" alt="">
					<span class="navigation-stages__line"></span>
					<div class="navigation-stages__action-stage">
						<span class="navigation-stages__action-stage-title">Этап</span>
						<span class="navigation-stages__action-stage-num">3</span>
						<span class="navigation-stages__action-stage-completed">(завершен)</span>
					</div>
				</div>
			</div>
			<div class="example-work-bloger">
				<h2 class="example-work-bloger__title">Весь макияж только губной помадой</h2>
				<div class="example-work-bloger__work">
					<div class="example-work-bloger__video">
						<iframe width="100%" height="295" src="https://www.youtube.com/embed/lWA2pjMjpBs" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="example-work-bloger__wrap-desc">
						<span class="example-work-bloger__bloger-name">Диана Глостер</span>
						<p class="example-work-bloger__desc">
							Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.
						</p>
						<div class="example-work-bloger__wrap-like">
							<span class="example-work-bloger__count-like">1012</span>
							<img class="example-work-bloger__icon-like" src="img/icon-like-grey.png" alt="">
						</div>
						<form class="example-work-bloger__form" action="">
							<div class="example-work-bloger__wrap-comment">
								<img class="example-work-bloger__commentator" src="img/commentator.png" alt="">
								<label class="example-work-bloger__lable-comment">
									<input type="text" name="comment" class="example-work-bloger__comment" placeholder="оставить комментарии">
								</label>
							</div>
							<div class="example-work-bloger__buttons">
								<button class="example-work-bloger__comments-all" href="">Все комментарии</button>
								<button class="example-work-bloger__send-comment" href="">Отправить комментарий</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="title-winner-stage">
			<div class="title-winner-stage__text">
				<img class="title-winner-stage__icon-crown" src="img/crown-winner.png" alt="">Победители этапа
			</div>
			<div class="title-winner-stage__line"></div>
		</div>
		<div class="container">
			<div class="works-winners-stage">
				<div class="works-winners-stage__wrap-item">
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
				</div>
				<div class="works-winners-stage__wrap-item">
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
				</div>
			</div>
		</div>

		<div class="title-participant-stage">
			<div class="title-participant-stage__text">Работы членов команды</div>
			<div class="title-participant-stage__line"></div>
		</div>
		<div class="container">
			<div class="works-winners-stage">
				<div class="works-winners-stage__wrap-item">
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
				</div>
				<div class="works-winners-stage__wrap-item">
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>

</body>
</html>