<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Avon</title>
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<script src="js/jquery-3.2.0.min.js"></script>
</head>
<body>
	<header class="header">		
		<div class="wrap-menu">
			<div class="wrap-logo">
				<a class="wrap-logo__link-logo" href="/">
					<img class="logo" src="img/logo.png" alt="logo">
				</a>
				<img class="wrap-logo__img-element" src="img/element-menu-1.png" alt="logo">
			</div>
			<div class="container-menu">
				<div class="menu">
					<img class="menu__img-back" src="img/element-menu-2.png" alt="">
					<img class="menu__icon-m1" src="img/m1.png" alt="">
					<div class="menu__timer">
						<div class="start-stage">до нового этапа осталось</div>
						<span class="menu__days">05</span>:
						<span class="menu__hours">12</span>:
						<span class="menu__minutes">45</span>:
						<span class="menu__seconds">13</span>
					</div>
					<nav class="menu__nav-sections nav-sections">
						<ul class="nav-sections__list">
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-brand" href="#">О бренде</a>
							</li>
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-rules" href="#">Правила</a>
							</li>
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-blogers" href="#">Блогеры</a>
								<ul class="nav-sections__submenu">
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Лиза Краснова</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Наташа Шелягина</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Диана Глостер</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Лена Блонда</a>
									</li>
								</ul>
							</li>
						</ul>
					</nav>
					<div class="menu__wrap-loupe">
						<img class="menu__loupe" src="img/loupe.png" alt="loupe">
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="wrap-container">
		<div class="container">
			<div class="container-elements">
				<img class="bloger-element-1" src="img/bloger-element-1.png" alt="">
				<img class="bloger-element-2" src="img/stage-competition-icon-5.png" alt="">
				<img class="bloger-element-3" src="img/element-decoration-6.png" alt="">
				<img class="bloger-element-5" src="img/element-decoration-3.png" alt="">
				<img class="bloger-element-6" src="img/element-decoration-7.png" alt="">
				<img class="bloger-element-7" src="img/element-video-6.png" alt="">
			</div>
			<form class="form-search">
				<div class="form-search__wrap">
					<input class="form-saerch__input" type="search">
					<button class="form-search__submit" type="submit"></button>
				</div>
			</form>
		</div>
		<div class="title-bloger">
			<div class="title-bloger__text">Блогеры:<span class="title-bloger__num">2</span></div>
			<div class="title-bloger__line"></div>
		</div>
		<div class="list-blogers">
			<div class="item-bloger item-bloger_lk">
				<div class="container">
					<div class="item-bloger__wrap-img">
						<img class="item-bloger__img" src="img/bloger-lk.png" alt="">
					</div>
					<div class="item-bloger__wrap-desc">
						<div class="item-bloger__wrap-name">
							<span class="item-bloger__specialty">бьюти блогер</span>
							<span class="item-bloger__first-name">Лиза</span>
							<span class="item-bloger__last-name">Краснова</span>
						</div>
						<p class="item-bloger__desc">
							Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.
						</p>
					</div>
				</div>
			</div>
			<div class="item-bloger item-bloger_lb">
				<div class="container">
					<div class="item-bloger__wrap-img">
						<img class="item-bloger__img" src="img/bloger-lb.png" alt="">
					</div>
					<div class="item-bloger__wrap-desc">
						<div class="item-bloger__wrap-name">
							<span class="item-bloger__specialty">бьюти блогер</span>
							<span class="item-bloger__first-name">Лена</span>
							<span class="item-bloger__last-name">Блонда</span>
						</div>
						<p class="item-bloger__desc">
							Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="title-video">
			<div class="title-video__text">Видео:<span class="title-video__num">4</span></div>
			<div class="title-video__line"></div>
		</div>
		<div class="container">
			<div class="works-winners-stage">
				<div class="works-winners-stage__wrap-item">
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen=""></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen=""></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
				</div>
				<div class="works-winners-stage__wrap-item">
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen=""></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
					<article class="works-winners-stage__item">
						<div class="works-winners-stage__video">
							<iframe width="248" height="140" src="https://www.youtube.com/embed/CGyEd0aKWZE" frameborder="0" allowfullscreen=""></iframe>
						</div>
						<h3 class="works-winners-stage__name">Анна Кременко</h3>
						<p class="works-winners-stage__desc">Весь макияж только хайлайтером</p>
						<div class="works-winners-stage__wrap-like">
							<span class="works-winners-stage__count-like">143</span>
							<img class="works-winners-stage__icon-like" src="img/icon-like-grey.png">
						</div>
					</article>
				</div>
			</div>
		</div>
		<div class="title-photo">
			<div class="title-photo__text">Фото:<span class="title-photo__num">1</span></div>
			<div class="title-photo__line"></div>
		</div>
		<div class="container">
			<div class="wrap-photos">
				<img class="wrap-photos__photo" src="img/makeup-photo-1.png" alt="">
			</div>
		</div>
	</div>
</body>
</html>