<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Avon</title>
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<script src="js/jquery-3.2.0.min.js"></script>
</head>
<body>
	<header class="header">		
		<div class="wrap-menu">
			<div class="wrap-logo">
				<a class="wrap-logo__link-logo" href="/">
					<img class="logo" src="img/logo.png" alt="logo">
				</a>
				<img class="wrap-logo__img-element" src="img/element-menu-1.png" alt="logo">
			</div>
			<div class="container-menu">
				<div class="menu">
					<img class="menu__img-back" src="img/element-menu-2.png" alt="">
					<img class="menu__icon-m1" src="img/m1.png" alt="">
					<div class="menu__timer">
						<div class="start-stage">до нового этапа осталось</div>
						<span class="menu__days">05</span>:
						<span class="menu__hours">12</span>:
						<span class="menu__minutes">45</span>:
						<span class="menu__seconds">13</span>
					</div>
					<nav class="menu__nav-sections nav-sections">
						<ul class="nav-sections__list">
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-brand" href="#">О бренде</a>
							</li>
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-rules" href="#">Правила</a>
							</li>
							<li class="nav-sections__item">
								<a class="nav-sections__link nav-sections__link-blogers" href="#">Блогеры</a>
								<ul class="nav-sections__submenu">
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Лиза Краснова</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Наташа Шелягина</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Диана Глостер</a>
									</li>
									<li class="nav-sections__submenu-item">
										<a class="nav-sections__submenu-link" href="#">Лена Блонда</a>
									</li>
								</ul>
							</li>
						</ul>
					</nav>
					<div class="menu__wrap-loupe">
						<img class="menu__loupe" src="img/loupe.png" alt="loupe">
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="wrap-container">
		<div class="container">
			<div class="container-elements">
				<img class="bloger-element-1" src="img/bloger-element-1.png" alt="">
				<img class="download-work-element-1" src="img/element-decoration-6.png" alt="">
				<img class="download-work-element-2" src="img/element-video-6.png" alt="">
				<img class="download-work-element-3" src="img/bottom-elment-2.png">
			</div>
			<div class="connect-team">
				<h2 class="connect-team__title">Присоединится к команде</h2>
				<div class="connect-team__bloger">
					<div class="connect-team__wrap-bloger-name">
						<span class="connect-team__bloger-name">Лиза Краснова</span>
					</div>
					<div class="connect-team__bloger-line"></div>
				</div>
				<div class="connect-team__stage">
					<div class="connect-team__stage-wrap-name">
						<span class="connect-team__stage-name">Этап</span>
						<span class="connect-team__stage-num">5</span>:
					</div>
					<h3 class="connect-team__stage-title">Весь макияж только хайлайтером</h3>
				</div>
				<div class="connect-team__conditions">
					<h2 class="connect-team__conditions-desc">Загрузи свое видео, участвуй в челендже и выграй приз от</h2>
					<img class="connect-team__conditions-logo" src="img/logo-2.png" alt="">
				</div>
			</div>
		</div>
		<div class="block-reg-team">
			<div class="container">
				<div class="block-reg-team__wrap-form">
					<form class="block-reg-team__form" enctype="multipart/form-data" method="post" action="">
						<div class="block-reg-team__wrap-download">
							<div class="block-reg-team__download">					
								<label class="block-reg-team__form-input-file" for="input-download">Загрузить фото<br> с компьютера</label>
								<input id="input-download" type="file" style="display: none;">
								<p class="block-reg-team__download-desc">вес файла до 5 МБ</p>
							</div>
						</div>
						<div class="block-reg-team__user-data">
							<h4 class="block-reg-team__form-desc">Вы можете загрузить ТОЛЬКО ОДНО фото или видео в этап</h4>					
							<input class="block-reg-team__form-input" type="text" placeholder="Ваше имя">
							<input class="block-reg-team__form-input" type="text" placeholder="Телефон">
							<input class="block-reg-team__form-input" type="text" placeholder="E-mail">
							<input class="block-reg-team__form-input" type="text" placeholder="Название фото">
							<textarea class="block-reg-team__form-textarea" name="" id="" placeholder="Описание к фото"></textarea>
							<button class="block-reg-team__form-submit" type="submit">Отправить</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>